const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance:{
    app: 'node-api', 
    hostName: 'localhost', 
    ipAddr: '127.0.0.1', 
    port: {
      $: 3000, 
      '@enabled': true,
        }, 
    vipAddress: 'node-api', 
    dataCenterInfo:{
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true, 
    fetchRegistry: true, 
    statusPageUrl: 'localhost:3000/info'
  }, 
  eureka:{
    host: 'localhost', 
    port:'8761',
    servicePath: '/eureka/apps/'
  },
});

module.exports = {
  connect: function connectToEureka() {
    client.start(error => {
      console.log(error || "NodeJs Eureka Started ! ");
    });
  }
}

